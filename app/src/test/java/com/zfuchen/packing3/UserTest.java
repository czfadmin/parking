/*
 * Copyright (c) 2019.
 */

package com.zfuchen.packing3;

import com.zfuchen.packing3.mock.User;

import org.junit.Test;

import java.util.List;

public class UserTest {
	private static void accept(User it) {System.out.println(it.getName());}
	
	@Test
	public void testCreateUsersList(){
		List<User> users=User.Companion.createUsersList(10);
		users.forEach(UserTest::accept);
	}
}
