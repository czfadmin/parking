package com.zfuchen.packing3.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class MainViewPagerAdapter extends FragmentPagerAdapter {
	private List<Fragment> fms = new ArrayList<>();
	
	public MainViewPagerAdapter(FragmentManager fm) {
		super(fm);
	}
	
	@NonNull
	@Override
	public Fragment getItem(int position) {
		return fms.get(position);
	}
	
	public void addFragment(Fragment f) {
		fms.add(f);
	}
	
	@Override
	public int getCount() {
		return fms.size();
	}
	@Override
	public void destroyItem(@NotNull ViewGroup container, int position, Object object){
		super.destroyItem(container, position, object);
	}
}
