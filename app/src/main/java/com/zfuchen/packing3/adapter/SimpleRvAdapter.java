package com.zfuchen.packing3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zfuchen.packing3.R;
import com.zfuchen.packing3.mock.User;

import java.util.List;

public class SimpleRvAdapter extends RecyclerView.Adapter<SimpleRvAdapter.ViewHolder> {
	private List<User> mUsers;
	
	public SimpleRvAdapter(List<User> mUsers) {
		this.mUsers = mUsers;
	}
	
	//	onCreateViewHolder扩充项目布局并创建持有者
	@NonNull
	@Override
	public SimpleRvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
	                                                     int viewType) {
		Context context = parent.getContext();
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.rv_simple_item_layout, parent, false);
		return new ViewHolder(view);
	}
	
	//	onBindViewHolder根据数据设置视图属性
	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		User user = mUsers.get(position);
		TextView textView = holder.tvUserName;
//		ImageView imageView = holder.ivToDetail;
		textView.setText(user.getName());
		
	
	}
	
	//	并getItemCount确定项目数
	@Override
	public int getItemCount() {
		return mUsers.size();
	}
	
	public static class ViewHolder extends RecyclerView.ViewHolder {
		public TextView tvUserName;
		public ImageView ivToDetail;
		
		public ViewHolder(@NonNull View itemView) {
			super(itemView);
			tvUserName = itemView.findViewById(R.id.tv_username);
			ivToDetail = itemView.findViewById(R.id.iv_to_detail);
		}
	}
	
}
