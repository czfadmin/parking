package com.zfuchen.packing3;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.zfuchen.packing3.adapter.MainViewPagerAdapter;
import com.zfuchen.packing3.fragment.HomeFragment;
import com.zfuchen.packing3.fragment.MeFragment;
import com.zfuchen.packing3.ui.SearchResultActivity;
import com.zfuchen.packing3.ui.setting.SettingsActivity;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {
	private MainViewPagerAdapter mainViewPagerAdapter;
	private BottomNavigationView navView;
	private  MenuItem preMenuItem;

	
	private ViewPager mViewPager;
	private BottomNavigationView.OnNavigationItemSelectedListener
			mOnNavigationItemSelectedListener
			= new BottomNavigationView.OnNavigationItemSelectedListener() {
		
		@Override
		public boolean onNavigationItemSelected(@NonNull MenuItem item) {
			switch (item.getItemId()) {
				case R.id.navigation_home:
					mViewPager.setCurrentItem(0);
					return true;
				case R.id.navigation_notifications:
					mViewPager.setCurrentItem(1);
					return true;
			}
			return true;
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		navView = findViewById(R.id.nav_view);
		navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
		init();
	}
	
	private void init() {
		mainViewPagerAdapter = new MainViewPagerAdapter(getSupportFragmentManager());
		mainViewPagerAdapter.addFragment(new HomeFragment());
		mainViewPagerAdapter.addFragment(new MeFragment());
		mViewPager = findViewById(R.id.main_view_pager);
		mViewPager.setAdapter(mainViewPagerAdapter);
		mViewPager.addOnPageChangeListener(this);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.toolbar_menu, menu);
//		searchView.setIconified(false);
//		searchView.setQueryHint("你想停在哪个地方");
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent;
		switch (item.getItemId()) {
			case R.id.menu_search:
				intent= new Intent(MainActivity.this, SearchResultActivity.class);
				startActivity(intent);
				break;
			case R.id.menu_setting:
				intent = new Intent(this, SettingsActivity.class);
				startActivity(intent);
				break;
		}
		return true;
	}
	
	@Override
	public void onPageScrolled(int position, float positionOffset,
	                           int positionOffsetPixels) {
		
	}
	
	@Override
	public void onPageSelected(int position) {
		if(preMenuItem!=null){
			preMenuItem.setChecked(false);
		}else{
			navView.getMenu().getItem(0).setChecked(false);
		}
		navView.getMenu().getItem(position).setChecked(true);
		preMenuItem= navView.getMenu().getItem(position);
	}
	
	@Override
	public void onPageScrollStateChanged(int state) {
	
	}
}
