package com.zfuchen.packing3.ui

import android.os.Bundle
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.zfuchen.packing3.R
import com.zfuchen.packing3.adapter.SimpleRvAdapter
import com.zfuchen.packing3.mock.User
import kotlinx.android.synthetic.main.activity_search_result.*

class SearchResultActivity : AppCompatActivity() {
	private var searchView: SearchView? = null
	private lateinit var mUsers: List<User>
	private var VISIBLE: Boolean = false
	
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_search_result)
		init()
	}
	
	override fun onCreateOptionsMenu(menu: Menu?): Boolean {

		val inflater = menuInflater
		inflater.inflate(R.menu.search_bar, menu)
		val searchItem = menu!!.findItem(R.id.ab_search)
		searchView = MenuItemCompat.getActionView(searchItem) as SearchView
		searchView!!.setOnSearchClickListener(object : View.OnClickListener {
			override fun onClick(v: View?) {
			}
		})
		//设置搜索框中的提示文字
		searchView!!.queryHint = "你想停在哪个地方"
		//设置搜索框有字时显示叉叉，无字时隐藏叉叉
		searchView!!.onActionViewExpanded()
		searchView!!.isIconified = true
		//设置搜索框里的文字样式
		val searchAutoComplete: SearchView.SearchAutoComplete = searchView!!.findViewById(R.id.search_src_text) as SearchView.SearchAutoComplete
		searchAutoComplete.setTextColor(resources.getColor(android.R.color.background_light))
		val searchEditFrame: LinearLayout = searchView!!.findViewById(R.id.search_edit_frame) as LinearLayout
		val params = searchEditFrame.layoutParams as ViewGroup.MarginLayoutParams
		params.leftMargin = 0
		params.rightMargin = 0
		searchEditFrame.layoutParams = params
		searchView!!.maxWidth = android.R.attr.maxWidth
		search_toolbar.setNavigationOnClickListener {
			if (searchAutoComplete.isShown) {
				try {
					searchAutoComplete.run { setText("") }
					val method = searchView!!.javaClass.getDeclaredMethod("onCloseClicked")
					method.isAccessible = true
					if (!VISIBLE) {
						VISIBLE = true
						rv_recommended_res.visibility = View.VISIBLE
						tv_flag.text = "根据你的所在的位置推荐的车位"
					}
					method.invoke(searchView)
				} catch (e: Exception) {
					e.printStackTrace()
				}
				
			} else {
				finish()
			}
		}
		
		// 打开搜索框的按钮触发事件监听
		searchView!!.setOnSearchClickListener {
			rv_recommended_res.visibility = View.INVISIBLE
//			tv_flag.text=resources.getText(R.string.rec_parking_space)
			tv_flag.text = "推荐"
			VISIBLE = false
		}
		return super.onCreateOptionsMenu(menu)
	}
	
	private fun init() {
		setSupportActionBar(search_toolbar)
		supportActionBar!!.setDisplayHomeAsUpEnabled(true)
		supportActionBar!!.setDisplayShowTitleEnabled(false)
		mUsers = User.createUsersList(20)
		val simpleRvAdapter = SimpleRvAdapter(mUsers)
		rv_recommended_res.adapter = simpleRvAdapter
		rv_recommended_res.layoutManager = LinearLayoutManager(this)
		
	}
	
	override fun onMenuOpened(featureId: Int, menu: Menu?): Boolean {
		if (menu != null) {
			if (menu.javaClass.simpleName == "MenuBuilder") {
				try {
					val method = menu.javaClass.getDeclaredMethod("setOptionalIconsVisible", Boolean.javaClass)
					method.isAccessible = true
					method.invoke(menu, true)
				} catch (e: Exception) {
					e.printStackTrace()
				}
			}
		}
		return super.onMenuOpened(featureId, menu)
	}
	
}
