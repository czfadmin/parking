package com.zfuchen.packing3.fragment;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baidu.mapapi.map.MapView;
import com.zfuchen.packing3.R;
import com.zfuchen.packing3.viewmodel.HomeViewModel;

public class HomeFragment extends Fragment {
	private MapView mMapView = null;
	private HomeViewModel mViewModel;
	
	public static HomeFragment newInstance() {
		return new HomeFragment();
	}
	
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
	                         @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		mMapView =getActivity().findViewById(R.id.bmapView);
		View view=inflater.inflate(R.layout.home_fragment , container, false);
		mMapView=(MapView) view.findViewById(R.id.bmapView);
		mMapView.showScaleControl(true);
		Log.d("TAG", "onCreateView: .......");
//		mMapView.showScaleControl(mViewModel.enableShowScaleControl);
		return view;
		
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d("TAG", "onActivityCreated: .......");
		mViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
		// TODO: Use the ViewModel
	}
	@Override
	public void onResume() {
		super.onResume();
		mMapView.onResume();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mMapView.onDestroy();
	}
	@Override
	public void onPause(){
		super.onPause();
		mMapView.onPause();
	}
	
}
