/*
 * Copyright (c) 2019.
 */

package com.zfuchen.packing3.mock

import kotlin.random.Random

data class User(var name: String, var salary: Float) {
	companion object {
		fun createUsersList(count: Int): List<User> {
			val users = mutableListOf<User>()
			(1..count).map { users.add(User("Person $it", Random(10).nextFloat())) }
			return users
		}
	}
}